# Makefile for Arduino projects
# SKETCHBOOK should be set in cdtools function (see docs/arduion.sh):
# http://www.graphics-muse.org/wiki/pmwiki.php?n=Cdtools.Cdtools
# Depends on Arduino Makefile setup:
# http://ed.am/dev/make/arduino-mk
# -------------------------------------------
BOARD = uno
SCREENRC=~/.screenrc.arduino
include $(SKETCHBOOK)/arduino.mk

