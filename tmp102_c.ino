// TMP102 Reader
// by Michael J. Hammel <http://www.graphics-muse.org>
// based on Wire Master Reader
// by Nicholas Zambetti <http://www.zambetti.com>
// Wire Library: http://arduino.cc/en/Reference/Wire
// Arduino Uno pinouts: http://forum.arduino.cc/index.php?topic=84190.0

// TMP102 is a two-wire temperature sensor.
// https://www.sparkfun.com/products/11931

// Created 13 November 2013

// This example code is in the public domain.

#include <Wire.h>

/* LED pin */
int led = 13;

/*
 * RD/WR define the addresses for setting up a Read or Write, respectively.
 * These addresses assume that ADD0 on the SparkFun breakout and TMP102 is
 * tied to the GND pin.  See pg 10 of the Datasheet (Serial Bus Address) to
 * determine other possible addresses.
 */
#define BUSADDR  0x48  // Tied to Ground pin - we use the 7 bit values as an address.

/*
 * Register Pointer for TMP102 temperature register.
 * See pg 5 of the Datasheet (Pointer Register).
 */
#define TEMP_REG    0x00 

// Global read state: 0=disabled, 1=enabled
byte readState = 0;

// Global temperature registers
byte temp[2];
int temperature;

// Tell the TMP102 we want to read from the temperature register.
void setRegister()
{
    byte rc;

    // Start talking to TMP102 and tell it we're gonna write something.
    Wire.beginTransmission(BUSADDR);

    // Write the register we want to talk to: the temperature register.
    rc = Wire.write(TEMP_REG);
    if ( rc != 1 )
	{
        Serial.println("Failed to write TEMP_REG");
		return;
	}

    // Stop the conversation and make sure it went okay.
    rc = Wire.endTransmission();
	switch(rc) 
	{
		case 0: readState = 1; break;
		case 1: Serial.println("Data too long"); break;
		case 2: Serial.println("Received NACK on addr"); break;
		case 3: Serial.println("Received NACK on data"); break;
		case 4: Serial.println("Unknown error"); break;
	}
}

// Start the read process.
void readTemp()
{
    byte bytesRead;
    byte idx;

    // Get two bytes, then send a stop bit.
    bytesRead = Wire.requestFrom(BUSADDR, 2, false);

    // Make sure we got just two bytes
    if ( Wire.available() < 2)
	{
        Serial.print("Short read: ");
        Serial.println(bytesRead);
	}
    else if ( Wire.available() > 2)
	{
        Serial.print("Long read: ");
        Serial.println(bytesRead);
	}
    	Serial.println("xfer bytes");
        // Now pull those two bytes into our space.
        idx = 0;
        while (Wire.available())
        {
    	    Serial.println("getting byte");
            temp[idx++] = Wire.read();
            if ( idx == 2 )
                break;
        }
    	Serial.print("Low: ");
    	Serial.println(temp[0]);
    	Serial.print("High: ");
    	Serial.println(temp[1]);
}

/*
 * Adjust the inbound data based on TMP102 register usage.
 * This is taken from the ATmega328 sample code from SparkFun.com
 * Also: see pg 6 of the Datasheet.
 * https://www.sparkfun.com/datasheets/Sensors/Temperature/tmp102.pdf
 */
void fixTemp()
{
    /* Stuff the two bytes we read into a single variable */
    temperature = (temp[0]<<8) | temp[1];

    /*
     * The TMP102 temperature registers are left justified. 
     * we need to right justify them.
     */
    temperature >>= 4; 

    /*
     * The TMP102 uses twos compliment for negative nubmers.
     * But the sign bit is in the wrong spot after the above shift.
     * Test for it and correct, if needed
     */
    if(temperature & (1<<11))
        temperature |= 0xF800; //Set bits 11 to 15 to 1s to get this reading into real twos compliment

    /*
     * Now convert to celcius.  One LSB = 0.0625 degrees Celcius.  So we can multiple the temp
     * variable by that amount or, equivalently, divide it by 16.  Divide by 16 == rightshift by 4
     * but that doesn't work with negative nubmers so we use the divide, which should be faster
     * than the multiply.
     */
    temperature /= 16;
}

void setup()
{
    Serial.begin(9600);  // start serial output to pass via USB
    pinMode(led, OUTPUT);// Enable LED used to show progress.
    Wire.begin();        // join i2c bus

    // Initialize temperature variable
    temp[0] = 0;
    temp[1] = 0;

    /* 
     * Tell the TMP102 we're going to be reading
     * from the temperature register.
     */
    // setRegister();          
	readState = 1;

    // Tell the other end of the serial connection about our status.
/*
    Serial.print("TMP102 run state: ");
    if ( readState )
        Serial.println("enabled");
    else
        Serial.println("disabled");
*/
}

/*
 * The main loop uses delay() because we don't need to be constantly
 * reading the temperature sensor.  We pause a bit between each read.
 */
void loop()
{
    /* Turn the LED on */
    digitalWrite(led, HIGH);

    // Don't read the temperature if we're not enabled.
    if ( readState )
    {
        readTemp();
        fixTemp();
        Serial.print("Temperature: ");
        Serial.print(temperature, DEC);
        Serial.println("C");
    }
    delay(1000);

    /* Turn the LED off */
    digitalWrite(led, LOW);
    delay(1000);
}
